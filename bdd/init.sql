use Animaux;
create table races (id_race INTEGER PRIMARY KEY NOT NULL, nom_race varchar(50));
create table animaux (id_animal INTEGER PRIMARY KEY NOT NULL, nom varchar(50), couleur varchar(50), id_race INTEGER FOREIGN KEY REFERENCES races(id_race));

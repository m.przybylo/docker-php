use animaux;
create table races (id_race INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, nom_race varchar(50));
create table animaux (id_animal INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT, nom varchar(50), couleur varchar(50), id_race INTEGER, FOREIGN KEY (id_race) REFERENCES races(id_race));

